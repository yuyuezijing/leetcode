/////Consider boundaries; Write test cases; Consider null pointer (next.next)///////

//Question
//Given a linked list, reverse the nodes of a linked list k at a time and return its modified list. 
//If the number of nodes is not a multiple of k then left-out nodes in the end should remain as it is.
//You may not alter the values in the nodes, only nodes itself may be changed.
//Only constant memory is allowed.
//For example,
//Given this linked list: 1->2->3->4->5 
//For k = 2, you should return: 2->1->4->3->5 
//For k = 3, you should return: 3->2->1->4->5 

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int x) { val = x; }
 * }
 */
public class Solution {
    public ListNode ReverseKGroup(ListNode head, int k) {
        if(k < 2)
            return head;
        if(head == null)
            return head;
        ListNode a = head;
        ListNode aFore = null;
        ListNode b = head;
        ListNode bNext = b.next;
        int count = 0;
        ListNode newHead = head;
        bool first = true;
        while (b != null) 
        {
            b = b.next;
            if(b == null)
                continue;
            bNext = b.next;
            count ++;
            if(count == k-1)
            {
                Reverse(aFore, a, b, bNext);
                if(first)
                {
                    newHead = b;   //head changed!!!
                    first = false;
                }
                aFore = a;
                a = a.next;
                b = a;
                if(b == null)continue;
                bNext = b.next;
                count = 0;
            }
        }
        return newHead;
    }
    
    private void Reverse(ListNode startFore, ListNode start, ListNode end, ListNode endNext)
    {
        ListNode p = end;
        ListNode q = start;
        while(p != start)
        {
            while(q.next != p) //find the node before Node p
            {
                q = q.next;
            }
            p.next = q;
            p = q;
            q = start;
        }
        if(startFore != null)
            startFore.next = end;
        start.next = endNext;
    }
}